# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.4](https://gitlab.com/tjmonsi/lit-location/compare/v0.0.3...v0.0.4) (2020-08-30)


### Bug Fixes

* fix on saving to properties ([7c9dd38](https://gitlab.com/tjmonsi/lit-location/commit/7c9dd389ebab6174b1f66c856009e6d0137ecd9d))

### [0.0.3](https://gitlab.com/tjmonsi/lit-location/compare/v0.0.2...v0.0.3) (2020-08-30)


### Bug Fixes

* fix for webpack ([7d88c85](https://gitlab.com/tjmonsi/lit-location/commit/7d88c85eb2c85c59a5344642c745b67b59d1f85f))

### [0.0.2](https://gitlab.com/tjmonsi/lit-location/compare/v0.0.1...v0.0.2) (2020-08-30)


### Bug Fixes

* fix for class decorators fail on webpack ([7cdaf45](https://gitlab.com/tjmonsi/lit-location/commit/7cdaf455b4df20fa12b8f7e82a2c7016342e9661))

### 0.0.1 (2020-08-30)


### Features

* first commit ([8314423](https://gitlab.com/tjmonsi/lit-location/commit/8314423b281ee609943050f20eaedbc1f51008fa))
