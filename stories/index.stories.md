```js script
import { html } from '@open-wc/demoing-storybook';
import '../lit-location.js';

export default {
  title: 'LitLocation',
  component: 'lit-location',
  options: { selectedPanel: "storybookjs/knobs/panel" },
};
```

# LitLocation

A component for...

## Features:

- a
- b
- ...

## How to use

### Installation

```bash
yarn add lit-location
```

```js
import 'lit-location/lit-location.js';
```

```js preview-story
export const Simple = () => html`
  <lit-location></lit-location>
`;
```
